
	<div class="push"></div>
	<footer>
		<p>&copy; Paranormal Science Lab 2013</p>
	</footer>

    </div> <!-- /container -->

    
    <?php wp_footer(); ?>
    <?php if( is_page('contact') ){ ?>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/verify.js"></script>
	<?php }?>
  </body>
</html>