<form action="" method="get" class="navbar-form navbar-left" role="search">
    <div class="form-group">
        <input type="text" name="s" id="search" class="form-control" value="<?php the_search_query(); ?>" />
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</form>